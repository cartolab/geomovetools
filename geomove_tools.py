# -*- coding: utf-8 -*-

"""
/***************************************************************************
 GeoMoveTools
                                 A QGIS plugin
 This plugin installs GeoMove Tools
                              -------------------
        begin                : 2018-10-02
        copyright            : (C) 2018 by Cartolab
        email                : luipir@gmail.com
        email                : davidfernandezarango@hotmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

__author__ = 'Cartolab'
__date__ = '2018-10-02'
__copyright__ = '(C) 2018 by Cartolab'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

import os
import sys
import inspect

from qgis.core import QgsProcessingAlgorithm, QgsApplication
from .geomove_tools_provider import GeoMoveToolsProvider

cmd_folder = os.path.split(inspect.getfile(inspect.currentframe()))[0]

if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)


class GeoMoveToolsPlugin(object):

    def __init__(self):
        self.provider = GeoMoveToolsProvider()

    def initGui(self):
        QgsApplication.processingRegistry().addProvider(self.provider)

    def unload(self):
        # remove only if provider is available. For dependency reason
        # provier can be available but c++ part already deleted => try:catch
        try:
            QgsApplication.processingRegistry().removeProvider(self.provider)
        except Exception as ex:
            pass

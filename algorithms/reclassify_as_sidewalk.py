# -*- coding: utf-8 -*-

"""
***************************************************************************
    reclassify_as_sidewalk.py
    -------------------------
    begin                : August 2018
    copyright            : (C) 2018 by Luigi Pirelli
    email                : luipir at gmail dot com
    dev for              : https://cartolab.udc.es
    Project              : https://cartolab.udc.es/geomove
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

__author__ = 'Luigi Pirelli'
__date__ = 'August 2018'
__copyright__ = '(C) 2018, Luigi Pirelli'


from PyQt5.QtCore import QCoreApplication
from qgis.core import (QgsProcessing,
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterBand,
                       QgsProcessingException,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterField,
                       QgsProcessingParameterNumber,
                       QgsProcessingParameterRasterDestination,
                       QgsFeatureRequest,
                       QgsVectorLayer,
                       QgsFeature,
                       QgsMessageLog,
                       QgsProject,
                       QgsProcessingUtils)
import processing
import collections
from ..geomove_tools_algorithm import GeoMoveAlgorithm


class ReclassifyAsSidewalk(GeoMoveAlgorithm):
    """Get a wathershed segmentized raster and classify sidewalk basing
    on a selection of categories that for sure belong to street and categories
    that can be probably sidewalks. If a sidewalk category is also contained in
    street categories it's is classified as street. Any street category will be
    set to a relative input value (if not set => NoData) and sidewalk categories
    will be classified with another relative input value.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    INPUT_RASTER = 'INPUT_RASTER'
    INPUT_RASTER_BAND = 'INPUT_RASTER_BAND'
    INPUT_PROBABLY_SIDEWALK_CATEGORIES_LAYER = 'INPUT_PROBABLY_SIDEWALK_CATEGORIES_LAYER'
    INPUT_PROBABLY_SIDEWALK_CATEGORIES_FIELD = 'INPUT_PROBABLY_SIDEWALK_CATEGORIES_FIELD'
    INPUT_SURE_STREET_CATEGORIES_LAYER = 'INPUT_SURE_STREET_CATEGORIES_LAYER'
    INPUT_SURE_STREET_CATEGORIES_FIELD = 'INPUT_SURE_STREET_CATEGORIES_FIELD'
    NEW_SIDEWALK_CATEGORY = 'NEW_SIDEWALK_CATEGORY'
    NEW_STREET_CATEGORY = 'NEW_STREET_CATEGORY'
    NEW_AMBIGUOUS_CATEGORY = 'NEW_AMBIGUOUS_CATEGORY'
    OVERLAP_CATEGORIES_LIMIT = 'OVERLAP_CATEGORIES_LIMIT'
    OUTPUT = 'RECLASSIFIED_OUTPUT_RASTER'

    def createInstance(self):
        self.messageTag = type(self).__name__ # e.g. string ReclassifyAsSidewalk
        return ReclassifyAsSidewalk()

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'reclassifyassidewalk'

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr('Reclassify as sidewalk')

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        # We add the input vector features source.
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                name=self.INPUT_RASTER,
                description=self.tr('Input raster'),
                defaultValue=None,
                optional=False
            )
        )
        self.addParameter(
            QgsProcessingParameterBand(
                name=self.INPUT_RASTER_BAND,
                description=self.tr('Input raster band'),
                defaultValue=1,
                parentLayerParameterName=self.INPUT_RASTER,
                optional=False
            )
        )
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                name=self.INPUT_PROBABLY_SIDEWALK_CATEGORIES_LAYER,
                description=self.tr('Probably sidewalk categories'),
                types=[QgsProcessing.TypeVector],
                defaultValue=None,
                optional=False
            )
        )
        self.addParameter(
            QgsProcessingParameterField(
                name=self.INPUT_PROBABLY_SIDEWALK_CATEGORIES_FIELD,
                description=self.tr('Probably sidewalk categories field'),
                defaultValue=None,
                parentLayerParameterName=self.INPUT_PROBABLY_SIDEWALK_CATEGORIES_LAYER,
                type=QgsProcessingParameterField.Any,
                allowMultiple=False,
                optional=False
            )
        )
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                name=self.INPUT_SURE_STREET_CATEGORIES_LAYER,
                description=self.tr('Sure street categories'),
                types=[QgsProcessing.TypeVector],
                defaultValue=None,
                optional=False
            )
        )
        self.addParameter(
            QgsProcessingParameterField(
                name=self.INPUT_SURE_STREET_CATEGORIES_FIELD,
                description=self.tr('Sure street categories field'),
                defaultValue=None,
                parentLayerParameterName=self.INPUT_SURE_STREET_CATEGORIES_LAYER,
                type=QgsProcessingParameterField.Any,
                allowMultiple=False,
                optional=False
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                name=self.NEW_SIDEWALK_CATEGORY,
                description=self.tr('Unique category for sidewalk'),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=1.0,
                optional=False
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                name=self.NEW_STREET_CATEGORY,
                description=self.tr('Unique category for street'),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=None,
                optional=True # in case not set => use NoData
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                name=self.NEW_AMBIGUOUS_CATEGORY,
                description=self.tr('Category used for ambiguous pixels'),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=3.0,
                optional=True # in case not set => use NoData
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                name=self.OVERLAP_CATEGORIES_LIMIT,
                description=self.tr('Overlap area limit to classify as sidewalk (m2)'),
                type=QgsProcessingParameterNumber.Integer,
                defaultValue=1,
                optional=True # 1n case not set doe not use this limit
            )
        )

        # We add a raster sink in which to store our processed raster (this
        # usually takes the form of a newly created raster layer when the
        # algorithm is run in QGIS).
        self.addParameter(
            QgsProcessingParameterRasterDestination(
                name=self.OUTPUT,
                description=self.tr('Output raster')
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        """
        Process a wathershed segmentized raster and classify sidewalk basing
        on a selection of categories that for sure belong to street and categories
        that can be probably sidewalks. If a sidewalk category is also contained in
        street categories it's is classified as street. Any street category will be
        set to a relative input value (if not set => NoData) and sidewalk categories
        will be classified with another relative input value.
        """

        # Retrieve the feature source and sink. The 'dest_id' variable is used
        # to uniquely identify the feature sink, and must be included in the
        # dictionary returned by the processAlgorithm function.
        input_raster = self.parameterAsRasterLayer(
            parameters,
            self.INPUT_RASTER,
            context
        )
        input_raster_band = self.parameterAsInt(
            parameters,
            self.INPUT_RASTER_BAND,
            context
        )
        nodata = input_raster.dataProvider().sourceNoDataValue(input_raster_band)

        input_probably_sidewalk_categories_layer = self.parameterAsVectorLayer(
            parameters,
            self.INPUT_PROBABLY_SIDEWALK_CATEGORIES_LAYER,
            context
        )
        if not input_probably_sidewalk_categories_layer or not input_probably_sidewalk_categories_layer.isValid():
            raise QgsProcessingException(self.invalidSourceError(parameters, self.INPUT_PROBABLY_SIDEWALK_CATEGORIES_LAYER))

        input_probably_sidewalk_categories_field = self.parameterAsFields(
            parameters,
            self.INPUT_PROBABLY_SIDEWALK_CATEGORIES_FIELD,
            context
        )
        input_probably_sidewalk_categories_field = input_probably_sidewalk_categories_field[0]

        input_sure_street_categories_layer = self.parameterAsVectorLayer(
            parameters,
            self.INPUT_SURE_STREET_CATEGORIES_LAYER,
            context
        )
        if not input_sure_street_categories_layer or not input_sure_street_categories_layer.isValid():
            raise QgsProcessingException(self.invalidSourceError(parameters, self.INPUT_SURE_STREET_CATEGORIES_LAYER))
        input_sure_street_categories_field = self.parameterAsFields(
            parameters,
            self.INPUT_SURE_STREET_CATEGORIES_FIELD,
            context
        )
        input_sure_street_categories_field = input_sure_street_categories_field[0]

        new_sidewalk_category = self.parameterAsDouble(
            parameters,
            self.NEW_SIDEWALK_CATEGORY,
            context
        )
        new_street_category = self.parameterAsDouble(
            parameters,
            self.NEW_STREET_CATEGORY,
            context
        )
        if not new_street_category:
            new_street_category = nodata

        new_ambiguous_category = self.parameterAsDouble(
            parameters,
            self.NEW_AMBIGUOUS_CATEGORY,
            context
        )
        overlap_category_limit = self.parameterAsInt(
            parameters,
            self.OVERLAP_CATEGORIES_LIMIT,
            context
        )

        # check inputs

        # If source was not found, throw an exception to indicate that the algorithm
        # encountered a fatal error. The exception text can be any string, but in this
        # case we use the pre-built invalidSourceError method to return a standard
        # helper text for when a source cannot be evaluated
        # if input_raster is None:
        #     raise QgsProcessingException(self.invalidSourceError(parameters, self.INPUT_RASTER))

        # gets outputs

        output_raster = self.parameterAsOutputLayer(
            parameters,
            self.OUTPUT,
            context
        )

        # Send some information to the user
        feedback.pushInfo('CRS is {}'.format(input_raster.crs().authid()))

        # gets the number of uniquevalues 

        # get all categories of the input raster
        ret = processing.run("native:rasterlayeruniquevaluesreport", {
                'INPUT':input_raster.source(),
                'BAND':input_raster_band,
                'OUTPUT_TABLE':'memory:'}, 
            context=context,
            feedback=feedback)
        all_input_raster_categories_layer = ret['OUTPUT_TABLE']

        # get all records from:0
        #   input_probably_sidewalk_categories_layer
        #   input_sure_street_categories_layer
        # get all feilds because if the values come from:
        #   native:rasterlayeruniquevaluesreport
        # means that if has the following fields that can be used to give a weight of probability:
        #   value (e.g. category)
        #   count (e.g occunecies of the value in the raster)
        #   m2
        sure_street_categories = {}
        probably_sidewalk_categories = {}

        probably_sidewalk_categories_field_index = input_probably_sidewalk_categories_layer.fields().indexFromName(input_probably_sidewalk_categories_field)
        sure_street_categories_field_index = input_sure_street_categories_layer.fields().indexFromName(input_sure_street_categories_field)

        request = QgsFeatureRequest()
        request.setFlags(QgsFeatureRequest.NoGeometry)
        request.setSubsetOfAttributes([input_probably_sidewalk_categories_field, 'count', 'm2'], input_probably_sidewalk_categories_layer.fields())
        for feat in input_probably_sidewalk_categories_layer.getFeatures(request):
            attributes = feat.attributes()
            category = attributes[probably_sidewalk_categories_field_index]
            probably_sidewalk_categories[category] = attributes

        request = QgsFeatureRequest()
        request.setFlags(QgsFeatureRequest.NoGeometry)
        request.setSubsetOfAttributes([input_sure_street_categories_field, 'count', 'm2'], input_sure_street_categories_layer.fields())
        for feat in input_sure_street_categories_layer.getFeatures(request):
            attributes = feat.attributes()
            category = attributes[sure_street_categories_field_index]
            sure_street_categories[category] = attributes
        
        # def dictionary where to store all category mapping rules
        categories_map = {}

        # map all street categories to ther new category field
        for street_cat in sure_street_categories.keys():
            categories_map[street_cat] = new_street_category;

        # now give a weight(e.g. new category) to sidewalk categories
        sidewalk_count_field_index = input_probably_sidewalk_categories_layer.fields().indexFromName('count')
        sidewalk_m2_field_index = input_probably_sidewalk_categories_layer.fields().indexFromName('m2')
        street_count_field_index = input_sure_street_categories_layer.fields().indexFromName('count')
        street_m2_field_index = input_sure_street_categories_layer.fields().indexFromName('m2')

        for sidewalk_category, sidewalk_attributes in probably_sidewalk_categories.items():
            if sidewalk_category in sure_street_categories.keys():
                # !!!not yet implemented!!!
                # check if both areas are enough small to safetly assign a the category as
                # a probably sidewalk
                # compare m2 fo the the categories and if they are "similar" (howto?) and less than a limit(how much?)
                # then assign a intermediate weight (need to set in input)
                # note: index 1 is 'count' field as in request
                if overlap_category_limit:
                    if sure_street_categories[sidewalk_category][street_m2_field_index] <= overlap_category_limit: # 1 M2
                        categories_map[sidewalk_category] = new_sidewalk_category
            else:
                categories_map[sidewalk_category] = new_sidewalk_category
        
        # all categories not sidewalk or street will gave a intermediate value
        for feat in all_input_raster_categories_layer.getFeatures():
            category = feat.attributes()[0]
            if category not in categories_map.keys():
                categories_map[category] = new_ambiguous_category

        # order cammping to allow a n ordered output in the log
        categories_map = collections.OrderedDict(sorted(categories_map.items()))

        # create and populate memory layer with the mapping table to be used with "Reclassify by layer" processign algorithm
        mapping_layer = QgsVectorLayer('none?crs=epsg:25829&field=category_min:double&field=category_max:double&field=new_category:double', 'mapping_layer', 'memory')
        if not mapping_layer.isValid():
            raise QgsProcessingException("Cannot mapping memory layer")

        mapping_layer.startEditing()
        for category, new_category in categories_map.items():
            feat = QgsFeature()
            feat.setFields(mapping_layer.fields())
            feat.setAttribute('category_min', category)
            feat.setAttribute('category_max', category)
            feat.setAttribute('new_category', new_category)
            mapping_layer.addFeature(feat)
        mapping_layer.commitChanges()
        QgsProject.instance().addMapLayer(mapping_layer, addToLegend=False)

        # reclassify raster basing on new map
        # all categoris not in probably_sidewalk_categories and not in sure_street_categories
        # will be assighed to "new_street_category"
        # a sample command is: processing.run("native:reclassifybylayer", {'INPUT_RASTER':'/path/to/raster.tif','RASTER_BAND':1,'INPUT_TABLE':'/path/to/vector_map.gpkg','MIN_FIELD':'value','MAX_FIELD':'value','VALUE_FIELD':'value','NO_DATA':-9999,'RANGE_BOUNDARIES':2,'NODATA_FOR_MISSING':True,'DATA_TYPE':1,'OUTPUT':'/path/to/output.tif'})
        res = processing.run("native:reclassifybylayer", {
            'INPUT_RASTER':input_raster.source(),
            'RASTER_BAND':input_raster_band,
            'INPUT_TABLE':mapping_layer.source(),
            'MIN_FIELD':'category_min',
            'MAX_FIELD':'category_max',
            'VALUE_FIELD':'new_category',
            'NO_DATA':nodata,
            'RANGE_BOUNDARIES':2, # min <= value <= max
            'NODATA_FOR_MISSING':True,
            'DATA_TYPE':5, # Float32
            'OUTPUT':output_raster
            }, 
            context=context,
            feedback=feedback
        )
        
        # clean temp memory layer
        QgsProject.instance().removeMapLayer(mapping_layer)
        QgsProject.instance().removeMapLayer(all_input_raster_categories_layer)

        # Return the results of the algorithm. In this case our only result is
        # the feature sink which contains the processed features, but some
        # algorithms may return multiple feature sinks, calculated numeric
        # statistics, etc. These should all be included in the returned
        # dictionary, with keys matching the feature corresponding parameter
        # or output names.
        return {self.OUTPUT: output_raster}
